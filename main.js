class Hamburger {
  constructor(size, stuffing) {
    try {
      if (arguments.length !== 2) {
        throw new HamburgerException("Здесь должно быть два аргумента!");
      }
      if (size !== Hamburger.SIZE_SMALL && size !== Hamburger.SIZE_LARGE) {
        throw new HamburgerException("Размер гамбургера указан неверно!");
      }
      if (
        stuffing !== Hamburger.STUFFING_CHEESE &&
        stuffing !== Hamburger.STUFFING_SALAD &&
        stuffing !== Hamburger.STUFFING_POTATO
      ) {
        throw new HamburgerException("Выберите начинку для бургера!");
      }

      this.size = size;
      this.stuffing = stuffing;
      this.toppings = [];
    } catch (e) {
      console.log(e);
    }
  }

  /// ============= Hamburger props  ============= \\\

  static SIZE_SMALL = { size: "small", price: 50, calories: 20 };
  static SIZE_LARGE = { size: "large", price: 100, calories: 40 };
  static STUFFING_CHEESE = { stuffing: "cheese", price: 10, calories: 20 };
  static STUFFING_SALAD = { stuffing: "salad", price: 20, calories: 5 };
  static STUFFING_POTATO = { stuffing: "potato", price: 15, calories: 10 };
  static TOPPING_MAYO = { topping: "mayo", price: 20, calories: 5 };
  static TOPPING_SPICE = { topping: "spice", price: 15, calories: 0 };

  /// ============= Methods ============= \\\

  addTopping(topping) {
    try {
      if (arguments.length === 0) {
        throw new HamburgerException("Нужно передать хотя бы 1 топпинг!");
      }
      if (
        topping !== Hamburger.TOPPING_MAYO &&
        topping !== Hamburger.TOPPING_SPICE
      ) {
        throw new HamburgerException("Выберите топпинг для гамбургера!");
      }

      for (let i = 0; i < arguments.length; i++) {
        this.toppings.push(arguments[i]);
      }
    } catch (e) {
      console.log(e);
    }
  }

  removeTopping(topping) {
    try {
      if (arguments.length !== 1) {
        throw new HamburgerException("Введите топпинг для удаления");
      }
      toppingIndex = this.toppings.indexOf(topping);
      this.toppings.splice(toppingIndex, 1);
    } catch (e) {
      console.log(e);
    }
  }

  getToppings() {
    return this.toppings;
  }

  getSize() {
    return this.size.size;
  }

  getStuffing() {
    return this.stuffing.stuffing;
  }

  calculatePrice() {
    const toppingsPrice = this.toppings.reduce(
      (total, topping) => total + topping.price,
      0
    );
    return this.size.price + toppingsPrice + this.stuffing.price;
  }

  calculateCalories() {
    const toppingsCalories = this.toppings.reduce(
      (total, topping) => total + topping.calories,
      0
    );
    return this.size.calories + toppingsCalories + this.stuffing.calories;
  }
}

/// ============= Custom Error ============= \\\

class HamburgerException extends Error {
  constructor(message) {
    super();
    this.name = "Hamburger Exception";
    this.message = message;
  }
}

/// ============= Task Result ============= \\\

const h = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
h.addTopping(Hamburger.TOPPING_MAYO);

console.log(h);
